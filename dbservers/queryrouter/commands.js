sh.addShard("glitterShard01/bulbasaur:27017")
sh.addShard("glitterShard02/charmander:27017")
sh.addShard("glitterShard03/squirtle:27017")

sh.enableSharding("glitter_bomb")

db.users.createIndex({ _id: "hashed" })
db.glamours.createIndex({ _id: "hashed" })
db.sparkles.createIndex({ _id: "hashed" })
db.sparkles.createIndex({ content: 'text' })
db.sparkles.createIndex({ attn: -1 })
db.sparkles.createIndex({ attn: -1, createdAt: -1 })
db.sparkles.createIndex( { "parent": 1 } )
db.sparkles.createIndex({ createdAt: -1 })

sh.shardCollection("glitter_bomb.users", { "_id": "hashed" } )
sh.shardCollection("glitter_bomb.glamours", { "_id": "hashed" } )
sh.shardCollection("glitter_bomb.sparkles", { "_id": "hashed" } )

use config
db.settings.save( { _id:"chunksize", value: 16 } ) // value is in MiB