# glitter-config

## Setup

### Local machine

Steps:
  1. sudo apt update
  2. sudo apt install software-properties-common
  3. sudo apt-add-repository ppa:ansible/ansible
  4. sudo apt update
  5. sudo apt install ansible
  6. sudo ansible-galaxy install rvm_io.ruby

### Remote machine

Steps:
  1. sudo apt update
  2. sudo apt install python

Note: Ansible requires python 2.7.x on the remote machine so do not alter this to python3

## Deploying

Assumption(s):
  - /repo/ is the directory where you have cloned this repository.
  - the ssh private key for the server you are about to connect to is located at ~/.ssh/id_rsa_glitter on your local machine.
  - the ssh private key for the git repo this script will clone from is located at ~/.ssh/id_rsa_glitter on your local machine.

Steps:
  1. ansible-playbook /repo/deploy/glitter-playbook.yml [hosts]
  2. mina setup SERVER=[IP ADDRESS | DOMAIN]
  3. mina deploy BRANCH=[BRANCH NAME] SERVER=[IP ADDRESS | DOMAIN]

Note: SERVER is REQUIRED. The default value for SERVER is null. The default value for BRANCH is master.

## Directories

### /webservers

Summary: Sets up the Ruby on Rails application

Warning: You should not need to do this. Only follow these steps if the Ansible playbook in /deploy is not working.

Assumption(s):
  - /repo/ is the directory where you have cloned this repository.
  - these steps are being performed on the remote server, not the local machine.

Steps:
  1. sudo mv /repo/webservers/unicorn@.service /etc/systemd/system/unicorn@.service
  2. sudo mv /repo/webservers/unicorn_glitter /etc/default/unicorn_glitter
  3. sudo mv /repo/webservers/glitter.conf /etc/nginx/sites-available/glitter.conf
  4. sudo ln -s /etc/nginx/sites-available/glitter.conf /etc/nginx/sites-enabled/glitter.conf
  5. sudo service nginx start 
  4. sudo systemctl enable unicorn@glitter.service
  5. sudo systemctl start unicorn@glitter.service

Note: Step 4 may require restart instead of start if you already have nginx running